#include <iostream>
#include <SFML/Graphics.hpp>

using namespace std;
using namespace sf;

class Game
{
private:
	int status = IN_MENU;
	Texture bgTexture;
	Texture carTextures[4];
	Sprite background;
	Sprite cars[2];
	int carMovementStep = 0;
public:
	static const int IN_MENU = 0;
	static const int IN_GAME = 1;
	Game();
	int getStatus();
	void startGame();
	void moveCars();
	void draw(RenderWindow &window);
};

Game::Game()
{
	bgTexture.loadFromFile("resources/bg.png");
	carTextures[0].loadFromFile("resources/carb.png");
	carTextures[1].loadFromFile("resources/carr.png");
	carTextures[2].loadFromFile("resources/carg.png");
	carTextures[3].loadFromFile("resources/cary.png");

	background.setTexture(bgTexture);

	cars[0].setTexture(carTextures[0]);
	cars[1].setTexture(carTextures[3]);
	
	for (int i = 0; i < 2; i++)
	{
		FloatRect gb = cars[i].getGlobalBounds();
		cars[i].setOrigin(Vector2f(gb.width / 2, gb.height / 2));
	}
	cars[0].setPosition(Vector2f(-100, 400));
	cars[1].setPosition(Vector2f(480, 500));

	cars[1].setRotation(270);
}

int Game::getStatus()
{
	return status;
}

void Game::startGame()
{
	status = IN_GAME;
}

void Game::moveCars()
{
	Vector2f pos = cars[0].getPosition();
	Vector2f velocity;
	switch (carMovementStep)
	{
		case 0:
		{
			if (pos.x >= 270)
				carMovementStep++;
			velocity = Vector2f(4, 0);
			break;
		}
		case 1:
		{
			if (pos.x > 320)
				carMovementStep++;
			cars[0].rotate(1.8);
			velocity = Vector2f(1, 1.5);
			break;
		}
		case 2:
		{
			if (pos.y > 700)
			{
				carMovementStep++;
				cars[0].setPosition(Vector2f(900, 220));
				srand(time(NULL));
				cars[0].setTexture(carTextures[rand() % 3]);
			}
			cars[0].setRotation(90);
			velocity = Vector2f(0, 4);
			break;
		}
		case 3:
		{
			if (pos.x < -100)
			{
				carMovementStep++;
				cars[0].setPosition(Vector2f(320, -100));
				srand(time(NULL));
				cars[0].setTexture(carTextures[rand() % 3]);
			}
			cars[0].setRotation(180);
			velocity = Vector2f(-4, 0);
			break;
		}
		case 4:
		{
			if (pos.y > 350)
				carMovementStep++;
			cars[0].setRotation(90);
			velocity = Vector2f(0, 4);
			break;
		}
		case 5:
		{
			if (pos.y > 400)
				carMovementStep++;
			cars[0].rotate(-1.8);
			velocity = Vector2f(1.5, 1);
			break;
		}
		case 6:
		{
			if (pos.x > 900)
			{
				carMovementStep = 0;
				cars[0].setPosition(Vector2f(-100, 400));
				cars[0].setRotation(0);
			}
			cars[0].setRotation(0);
			velocity = Vector2f(4, 0);
			break;
		}
	}
	cars[0].move(velocity);

	if (Keyboard::isKeyPressed(Keyboard::W))
	{
		cars[1].move(Vector2f(0, -4));
		cars[1].setRotation(270);
	}
	if (Keyboard::isKeyPressed(Keyboard::S))
	{
		cars[1].move(Vector2f(0, 4));
		cars[1].setRotation(90);
	}
	if (Keyboard::isKeyPressed(Keyboard::A))
	{
		cars[1].move(Vector2f(-4, 0));
		cars[1].setRotation(180);
	}
	if (Keyboard::isKeyPressed(Keyboard::D))
	{
		cars[1].move(Vector2f(4, 0));
		cars[1].setRotation(0);
	}

	Vector2f c1pos = cars[1].getPosition();
	if (cars[1].getPosition().x > 900)
		cars[1].setPosition(Vector2f(-100, c1pos.y));
	if (cars[1].getPosition().x < -100)
		cars[1].setPosition(Vector2f(900, c1pos.y));
	if (cars[1].getPosition().y > 700)
		cars[1].setPosition(Vector2f(c1pos.x, -100));
	if (cars[1].getPosition().y < -100)
		cars[1].setPosition(Vector2f(c1pos.x, 700));
}

void Game::draw(RenderWindow &window)
{
	window.draw(background);
	moveCars();
	window.draw(cars[0]);
	window.draw(cars[1]);
}

class MainMenu
{
private:
	const Color NOT_SELECTED_COLOR = Color::White;
	const Color SELECTED_COLOR = Color::Blue;
	RenderWindow* window;
	Game* game;
	Font font;
	Text options[2];
	int selected = 0;
public:
	MainMenu(RenderWindow* window, Game* game);
	void moveDown();
	void moveUp();
	void action();
	void draw();
};

MainMenu::MainMenu(RenderWindow* window, Game* game)
{
	this -> window = window;
	this -> game = game;
	font.loadFromFile("segoeui.ttf");
	
	FloatRect gb;
	
	options[0] = Text("Nowa gra", font);
	gb = options[0].getGlobalBounds();
	options[0].setOrigin(Vector2f(gb.width / 2, gb.height / 2));
	options[0].setPosition(Vector2f(400, 250));

	options[1] = Text("Wyjscie", font);
	gb = options[1].getGlobalBounds();
	options[1].setOrigin(Vector2f(gb.width / 2, gb.height / 2));
	options[1].setPosition(Vector2f(400, 350));

	options[0].setFillColor(SELECTED_COLOR);
	options[0].setStyle(Text::Bold);
}

void MainMenu::moveDown()
{
	options[selected].setFillColor(NOT_SELECTED_COLOR);
	options[selected].setStyle(Text::Regular);
	selected++;
	if (selected > 1)
		selected = 0;
	options[selected].setFillColor(SELECTED_COLOR);
	options[selected].setStyle(Text::Bold);
}

void MainMenu::moveUp()
{
	options[selected].setFillColor(NOT_SELECTED_COLOR);
	options[selected].setStyle(Text::Regular);
	selected--;
	if (selected < 0)
		selected = 1;
	options[selected].setFillColor(SELECTED_COLOR);
	options[selected].setStyle(Text::Bold);
}

void MainMenu::action()
{
	switch (selected)
	{
		case 0:
		{
			game->startGame();
			break;
		}
		case 1:
		{
			window->close();
			break;
		}
	}
}

void MainMenu::draw()
{
	window->draw(options[0]);
	window->draw(options[1]);
}

int main()
{
	RenderWindow window(VideoMode(800, 600), "Menu");
	window.setFramerateLimit(60);

	Game game;
	MainMenu mainMenu(&window, &game);

	while (window.isOpen())
	{
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				window.close();
			if (event.type == Event::KeyPressed)
			{
				if (event.key.code == Keyboard::Up)
					mainMenu.moveUp();
				if(event.key.code == Keyboard::Down)
					mainMenu.moveDown();
				if (event.key.code == Keyboard::Enter)
					mainMenu.action();
			}
		}
		window.clear();
		if (game.getStatus() == Game::IN_MENU)
			mainMenu.draw();
		else if (game.getStatus() == Game::IN_GAME)
			game.draw(window);
		window.display();
	}
}